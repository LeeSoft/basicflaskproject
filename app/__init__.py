from flask import Flask, render_template, redirect, request

app = Flask('MyProject')

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/hello', methods=['POST'])
def hello_action():
    print('Incoming request:', request.form)
    return render_template('hello_template.html', user_name = request.form['user_name'])
