from flask import Flask


class cfg:
    class server:
        # ip = '0.0.0.0' # Server will be available from LAN
        ip = '127.0.0.1'
        portNo = 5000
        debug = True


def getOwnIps() -> list:
    import socket
    return socket.gethostbyname_ex(socket.gethostname())


# TODO Why this code displays data twice?
if __name__ == '__main__':
    if cfg.server.ip == '0.0.0.0':
        serverAddresses = getOwnIps()
        serverAddresses = [serverAddresses[0]] + serverAddresses[2]
    else:
        serverAddresses = [cfg.server.ip]
    print('Server started at:')
    for serverAddress in serverAddresses:
        print(' * http://{}:{}'.format(serverAddress, cfg.server.portNo))

    app = Flask('MyProject')
    from app import * # Do not move this line higher!
    app.run(cfg.server.ip, port=cfg.server.portNo, debug=cfg.server.debug)
